from __future__ import division
import numpy as np
from hmmlearn import hmm

"""
Read Data
"""
X = np.loadtxt('orderedFeatures2.csv',delimiter=',')
Y = np.loadtxt('example_labels.csv', delimiter=',',skiprows=1)

"""
Initialize parameters
"""
n_cooefficients = 12
# Number of windows
n_windows = 40
# Unique number of heart sounds
numberofheartsounds = np.unique(Y[:,1])

"""
Train HMM Models
"""

HMMModels = []
for row in range(0,len(Y)):
    x = []
    start = 0
    for i in range(0,n_windows):
        end = start+n_cooefficients
        x.append(X[row,start:end])
        start = end
    model = hmm.GaussianHMM(n_components=2, covariance_type="diag", n_iter=1000)
    model.fit(x)
    HMMModels.append(model)


"""
Test data on HMM Models
"""

totalcount = 0
correctcount = 0
for row in range(0,len(Y)):
    Obs = []
    start = 0
    for i in range(0,n_windows):
        end = start+n_cooefficients
        Obs.append(X[row,start:end])
        start = end
    # Find the most probable Model
    scores = np.array([])
    for model in HMMModels:
    # Find log likelihood
    	score = model.score(Obs)
    	scores = np.append(scores, score)
    # Find out which model gives the max likelihood
    maxlikely = np.argmax(scores)
    predictedY = Y[maxlikely,1]
    totalcount = totalcount+1
    if predictedY == Y[row,1]:
        correctcount = correctcount+1

print correctcount, totalcount, float(correctcount/totalcount)*100