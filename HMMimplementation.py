from __future__ import division
import numpy as np
from hmmlearn import hmm
import sklearn

"""
Read Data
"""
X = np.loadtxt('trainorderedFeatures.csv',delimiter=',')
Y = np.loadtxt('train_labels.csv', delimiter=',')

testX = np.loadtxt('testorderedFeatures.csv',delimiter=',')
testY = np.loadtxt('test_labels.csv', delimiter=',')

"""
Initialize parameters
"""
n_cooefficients = 12
# Number of windows
n_windows = 40
# Unique number of heart sounds
numberofheartsounds = np.unique(Y[:,1])

"""
Train HMM Models
"""

HMMModels = []
for row in range(0,len(Y)):
    x = []
    start = 0
    for i in range(0,n_windows):
        end = start+n_cooefficients
        x.append(X[row,start:end])
        start = end
    model = hmm.GaussianHMM(n_components=2, covariance_type="diag", n_iter=1000)
    model.fit(x)
    HMMModels.append(model)


"""
Test data on HMM Models
"""

totalcount = 0
correctcount = 0
y_pred = []
y_true = []
for row in range(0,len(testY)):
    Obs = []
    start = 0
    for i in range(0,n_windows):
        end = start+n_cooefficients
        Obs.append(testX[row,start:end])
        start = end
    # Find the most probable Model
    scores = np.array([])
    for model in HMMModels:
    # Find log likelihood
    	score = model.score(Obs)
    	scores = np.append(scores, score)
    # Find out which model gives the max likelihood
    maxlikely = np.argmax(scores)
    predictedY = Y[maxlikely,1]
    totalcount = totalcount+1
#    print predictedY, testY[row,1], predictedY == testY[row,1], float(correctcount/totalcount)*100
    y_pred.append(predictedY)
    y_true.append(testY[row,1])
    if predictedY == testY[row,1]:
        correctcount = correctcount+1


print correctcount, totalcount, float(correctcount/totalcount)*100

#print y_pred
#print y_true
#print sklearn.metrics.confusion_matrix(y_true, y_pred)